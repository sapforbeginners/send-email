# README #

###Internal Tables###
p_it_body[] -> Body of the eMail.
p_it_attachment1[], p_it_attachment2[] -> Attachment1 and Attachment2 data in internal tables respectively with string type. Make sure to change the attachment type while adding to the add_attachment method.
p_it_email_list[] -> eMail addresses of the recipients in string format.
###Parameters###
p_subject -> Subject of the email
p_attachment1_title, p_attachment2_title -> Titles of the attachments respectively

 
### What is this repository for? ###

This repository is about sending an email from SAP ABAP using standard CL_BCS class. You can use this as a starting template to send email.

### How do I get set up? ###

Copy the subroutine "f_email_send" and call the subroutine with the required parameters.

### Contribution guidelines ###

Please let admin@sapforbeginners.com know about any tweaks or changes to improve this code.

### Who do I talk to? ###

admin@sapforbeginners.com