*& Report  ZS4B_EMAIL_SEND
*&
*&---------------------------------------------------------------------*
************************************************************************
* Source Code Documentation
************************************************************************
*
*  Program Name: ZS4B_EMAIL_SEND
*  Author:       Pavan Kumar P
*  Date:         18th February, 2015
*  Code Type:    New
*  Requester:    SAP for Beginners
*  Input:        Email Entries in TVARVC table
*  Output:       Email Report
*  FS ID:        N/A
*  Rev Trac:     N/A
*
*  PROGRAM PURPOSE:
*&---------------------------------------------------------------------*
*&  This program will provide a subroutine declaration for you to send *
*&  email. This subroutine receive e-mail body, two e-mail attachments *
*&  and list of email addresses along with email subject and attachment*
*&  subjects to trigger the email.                                     *
*&---------------------------------------------------------------------*
*  MODIFICATION LOG:
*  CORR.NO.     DATE      PROGRAMMER      VERSION   CHANGES DESCRIPTION
*----------------------------------------------------------------------.
*  Initial      18/02/15  Pavan Kumar, P    001     Initial Version    *
*&---------------------------------------------------------------------*
 
REPORT  ZS4B_EMAIL_SEND.
 
  DATA: it_body         TYPE STANDARD TABLE OF string,
        it_attachment1  TYPE STANDARD TABLE OF string,
        it_attachment2  TYPE STANDARD TABLE OF string,
        it_email_list   TYPE STANDARD TABLE OF string,
        it_email_tvarv  TYPE STANDARD TABLE OF tvarvc.
 
  DATA: wa_body         TYPE string,
        wa_email_list   TYPE string,
        wa_email_tvarv  TYPE tvarvc,
        lv_subject      TYPE string,
        lv_email        TYPE string VALUE 'Attachment1 Title',
        lv_error        TYPE string VALUE 'Attachment2 Title'.
 
  CONSTANTS: c_email    TYPE c LENGTH 20 VALUE 'ZC_EMAIL'.                       "I have maintained a TVARVC entry using STVARV entry for list of emails I need to send email
 
  SELECT *
    FROM tvarvc
    INTO TABLE it_email_tvarv
    WHERE name = c_email.
  IF sy-subrc NE 0.
    MESSAGE 'Email address entries are not maintained in TVARVC Table' TYPE 'I'.
    EXIT.
  ELSE.
    LOOP AT it_email_tvarv INTO wa_email_tvarv.
      APPEND wa_email_tvarv-low TO it_email_list.
    ENDLOOP.
  ENDIF.
 
* Fill Anything into it_attachment1[] and it_attachment2[] before proceeding
 
  CONCATENATE 'Email Subject' sy-datum sy-uzeit INTO lv_subject SEPARATED BY ' '.
  IF it_attachment1[] IS INITIAL AND it_attachment2[] IS INITIAL.
    wa_body = 'There are no attachments to send in the email'.
    APPEND wa_body TO it_body.
  ELSE.
    wa_body = 'Please find attached file(s) generated<br/>'.
    APPEND wa_body TO it_body.
    wa_body = '<br/><strong>Thanks and Regards</strong><br/>'.
    APPEND wa_body TO it_body.
    wa_body = 'SAP for Beginners'.
    APPEND wa_body TO it_body.
  ENDIF.
 
 
 
  PERFORM f_email_send TABLES it_body
                              it_attachment1
                              it_attachment2
                              it_email_list
                       USING  lv_subject
                              lv_error
                              lv_email.
 
 
*&---------------------------------------------------------------------*
*&      Form  F_EMAIL_SEND
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_IT_BODY  text
*      -->P_IT_EMAIL  text
*      -->P_IT_ERROR  text
*      -->P_IT_EMAIL_LIST  text
*----------------------------------------------------------------------*
FORM f_email_send  TABLES   p_it_body             TYPE STANDARD TABLE
                            p_it_attachment1      TYPE STANDARD TABLE
                            p_it_attachment2      TYPE STANDARD TABLE
                            p_it_email_list       TYPE STANDARD TABLE
                   USING    p_subject
                            p_attachment1_title
                            p_attachment2_title.
 
  CONSTANTS: c_at       TYPE c LENGTH 2   VALUE '@',
             c_u        TYPE c LENGTH 1   VALUE 'U',
             c_c        TYPE c LENGTH 1   VALUE 'C'.
  CONSTANTS: c_bin      TYPE c LENGTH 3   VALUE 'BIN',
             c_txt      TYPE c LENGTH 3   VALUE 'TXT',
             c_htm      TYPE c LENGTH 3   VALUE 'HTM',
             c_csv      TYPE c LENGTH 3   VALUE 'CSV'.
 
  DATA: lc_doc_bcs      TYPE REF TO             cl_document_bcs,                                        "Persistent Class for Document Send
        lc_bcs          TYPE REF TO             cl_bcs,                                                 "Class for Business Communication Service
        it_body         TYPE                    soli_tab,                                               "Body        Binary content
        it_attachment1  TYPE                    soli_tab,                                               "Attachment1 Binary content
        it_attachment2  TYPE                    soli_tab,                                               "Attachment2 Binary content
        lv_subject      TYPE                    so_obj_des,                                             "Subject of the email
        lc_sender       TYPE REF TO             if_sender_bcs,                                          "Interface for receiving Sender   in Business Communication Service class
        lc_recipient    TYPE REF TO             if_recipient_bcs,                                       "Interface for receiving receiver in Business Communication Service class
        lv_receiver     TYPE                    adr6-smtp_addr,                                         "Receiver Input of receiver Interface
        lv_newline      TYPE c VALUE            cl_abap_char_utilities=>newline.                        "Hexadecimal New line
 
 
  DATA: wa_body         TYPE string,                                                                    "Work Area for Body Content
        wa_attachment1  TYPE string,                                                                    "Work Area for Attachment1 Content
        wa_attachment2  TYPE string,                                                                    "Work Area for Attachment2 Content
        wa_email_list   TYPE string,                                                                    "Work Area for Email List Content
        lv_temp         TYPE string.                                                                    "Temporary Variable
 
  TRY.
      CREATE OBJECT lc_doc_bcs.
* Creating Persistent Object to set document in email
      lc_bcs = cl_bcs=>create_persistent( ).
 
*Moving Received Parameters to the Appropriate File type Variables.
 
      LOOP AT p_it_email_list INTO wa_email_list.
 
        lv_receiver = wa_email_list.
        lc_recipient = cl_cam_address_bcs=>create_internet_address( lv_receiver ).
        lc_bcs->add_recipient( lc_recipient ).
 
      ENDLOOP.
      IF p_it_body[] IS NOT INITIAL.
        CLEAR: lv_temp, lv_subject.
        MOVE p_subject TO lv_subject.
        LOOP AT p_it_body INTO wa_body.
          IF lv_temp IS INITIAL.
            CONCATENATE wa_body lv_newline INTO lv_temp.
          ELSE.
            CONCATENATE lv_temp wa_body lv_newline INTO lv_temp.
          ENDIF.
        ENDLOOP.
 
        CALL FUNCTION 'SCMS_STRING_TO_FTEXT'
          EXPORTING
            text      = lv_temp
          TABLES
            ftext_tab = it_body.
 
        lc_doc_bcs = cl_document_bcs=>create_document(
                                                        i_type       = c_htm
                                                        i_subject    = lv_subject
                                                        i_language   = sy-langu
                                                        i_importance = '1'
                                                        i_text       = it_body ).
      ENDIF.
*Attachment1
      IF p_it_attachment1[] IS NOT INITIAL.
        CLEAR: lv_temp, lv_subject.
        MOVE p_attachment1_title TO lv_subject.
        LOOP AT p_it_attachment1 INTO wa_attachment1.
          IF lv_temp IS INITIAL.
            CONCATENATE wa_attachment1 lv_newline INTO lv_temp.
          ELSE.
            CONCATENATE lv_temp wa_attachment1 lv_newline INTO lv_temp.
          ENDIF.
        ENDLOOP.
        CALL FUNCTION 'SCMS_STRING_TO_FTEXT'
          EXPORTING
            text      = lv_temp
          TABLES
            ftext_tab = it_attachment1.
 
        lc_doc_bcs->add_attachment(
                                    i_attachment_type     = c_txt
                                    i_attachment_subject  = lv_subject
                                    i_att_content_text    = it_attachment1 ).
      ENDIF.
*Attachment2
      IF p_it_attachment2[] IS NOT INITIAL.
        CLEAR: lv_temp, lv_subject.
        MOVE p_attachment2_title TO lv_subject.
        LOOP AT p_it_attachment2 INTO wa_attachment2.
          IF lv_temp IS INITIAL.
            CONCATENATE wa_attachment2 lv_newline INTO lv_temp.
          ELSE.
            CONCATENATE lv_temp wa_attachment2 lv_newline INTO lv_temp.
          ENDIF.
        ENDLOOP.
        CALL FUNCTION 'SCMS_STRING_TO_FTEXT'
          EXPORTING
            text      = lv_temp
          TABLES
            ftext_tab = it_attachment2.
 
        lc_doc_bcs->add_attachment(
                                    i_attachment_type     = c_txt
                                    i_attachment_subject  = lv_subject
                                    i_att_content_text    = it_attachment2 ).
 
      ENDIF.
 
      lc_bcs->set_document( lc_doc_bcs ).
      lc_sender = cl_sapuser_bcs=>create( sy-uname ).
      lc_bcs->set_sender( lc_sender ).
      lc_bcs->set_send_immediately( abap_true ).
      lc_bcs->send( ).
 
      COMMIT WORK AND WAIT.
* Catch the Exceptions if any and raise a message.
    CATCH cx_send_req_bcs.
      MESSAGE 'Error sending email due to system exception: CX_SEND_REQ_BCS' TYPE 'E'.
    CATCH cx_document_bcs.
      MESSAGE 'Error sending email due to system exception: CX_DOCUMENT_BCS' TYPE 'E'.
    CATCH cx_address_bcs.
      MESSAGE 'Error sending email due to system exception: CX_ADDRESS_BCS'  TYPE 'E'..
 
  ENDTRY.
 
ENDFORM.                    " F_EMAIL_SEND